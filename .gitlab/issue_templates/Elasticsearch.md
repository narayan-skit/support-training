---
module-name: "Elasticsearch"
area: "Core Technologies"
maintainers:
  - TBD
---

Title: "Elasticsearch - your-name"

## Goal of this checklist

> Set a clear path for Elasticsearch Expert training

## Objectives:

- Learn about Elasticsearch
- Learn about GitLab's Elasticsearch integration
- Feel comfortable answering some common scenarios.

---

### Stage 1: Commit and become familiar with what Elasticsearch is

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Elasticsearch questions to you
1. [ ] Read Elasticsearch Documentation
  - [ ] Read [What is Elasticsearch?](https://www.elastic.co/what-is/Elasticsearch)
  - [ ] Read [Elasticsearch Reference – Basic Concepts](https://www.elastic.co/guide/en/Elasticsearch/reference/current/getting-started-concepts.html)
  - [ ] Read [Elasticsearch Reference – Reading and Writing Documents](https://www.elastic.co/guide/en/Elasticsearch/reference/current/docs-replication.html)
  - [ ] Read [Elasticsearch Reference – Analysis](https://www.elastic.co/guide/en/Elasticsearch/reference/current/analysis.html)
1. [ ] Read the GitLab Documentation
  - [ ] Read [Elasticsearch integration](https://docs.gitlab.com/ee/integration/Elasticsearch.html)
  - [ ] Read [Troubleshooting Elasticsearch](https://docs.gitlab.com/ee/administration/troubleshooting/Elasticsearch.html)
  - [ ] Read [Elasticsearch lessons learnt for Advanced Global Search](https://about.gitlab.com/blog/2020/04/28/Elasticsearch-update/)

### Stage 2: Technical setup

1. [ ] Spin up an Elasticsearch installation. This can be via VM or docker.
1. [ ] Follow [Elasticsearch integration](https://docs.gitlab.com/ee/integration/Elasticsearch.html)
      to integrate said Elasticsearch installation with a running GitLab
      installation.
1. [ ] Practice indexing and re-indexing the whole GitLab instance.
1. [ ] Practice re-indexing a specific project.
1. [ ] Feel comfortable determining which projects are not indexed.

### Stage 3: Quiz

1. [ ] Contact a current Elasticsearch trainer and let them know you are ready
      for the quiz. They will provide you 3 scenarios in the comment section
      below. You will reply to each scenario as if it was a ticket. You should
      treat these as customer replies and attempt to fully resolve the
      scenario in **one*- reply. Example scenarios can be found
      [here](/content/module-Elasticsearch/scenarios.md).
1. [ ] Schedule a call with a current Elasticsearch trainer. During this call,
      you will guide them through the following:
  - [ ] Integrating a running Elasticsearch installation with GitLab. This
        does include fully indexing all your data. **Pro-tip**: Do it with
        a smaller data set to speed up the process.
  - [ ] Via the Elasticsearch API, pull the following:
    - [ ] The current health status of the Elasticsearch installation.
    - [ ] Information about the index you just created.
  - [ ] Clear the index status for one project and then re-index that project.
  - [ ] Perform a search for a basic term and verify the results are the same
        via the following methods:
    - [ ] The rails console
    - [ ] The Elasticsearch Search API
    - [ ] The GitLab Search UI
1. [ ] Once you have completed this, have the trainer comment below acknowledging your success.

### Final Stage:

1. [ ] Your Manager needs to check this box to acknowledge that you finished
1. [ ] Send a MR to declare yourself an Elasticsearch Expert on the team page

/label module
