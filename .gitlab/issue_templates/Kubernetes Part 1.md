---
module-name: "Kubernetes Part 1"
area: "Core Technologies"
maintainers:
  - TBD
---

**Title:** _"Kubernetes Module Series I: Introduction to Kubernetes - **your-name**"_

**Goal of this checklist:** Set a clear path for start of your Kubernetes journey.

This is a prerequisite to other Kubernetes modules in the series. Remember to contribute to any documentation that needs updating.

### Stage 0: Pre-Requisites

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Join the [#support_module-k8s](https://app.slack.com/archives/CRN64045C/) to co-ordinate with other learners
1. [ ] Get Invited to a recurrent Calendar event or create one in your region if none exists yet, to pair and learn together.

### Stage 1: Background Knowledge

This stage is meant to provide your with detailed background knowledge to get you started.

1. [ ] Follow Tutorial: [Kubernetes Basics](https://kubernetes.io/docs/tutorials/kubernetes-basics/)
1. [ ] Read [Kubernetes Concepts](https://kubernetes.io/docs/concepts/)
1. [ ] Take Linux Foundation's [Introduction to Kubernetes course](https://training.linuxfoundation.org/training/training-introduction-to-kubernetes/) (Choose the Audit version when asked. Premium is no use to us) 
1. [ ] [Study the Kubernetes Cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
1. [ ] [Helm QuickStart](https://helm.sh/docs/intro/quickstart/)
1. [ ] [Introduction to k3s](https://www.youtube.com/watch?v=FrGpJNI8na4)
    - [Try K3sup](https://github.com/alexellis/k3sup) a light-weight utility to get from zero to KUBECONFIG with k3s on any local or remote VM.
1. [ ] [Learn about Octant](https://octant.dev/) Octant is an open source developer-centric web interface for Kubernetes that lets you inspect a Kubernetes cluster and its applications. Extremely helpful while you're learning.
1. [ ] TroubleShooting Kubernetes
    - [Kubernetes.io Troubleshooting Documentation](https://kubernetes.io/docs/tasks/debug-application-cluster/troubleshooting/)
    - [A visual guide on troubleshooting Kubernetes deployments](https://learnk8s.io/troubleshooting-deployments)

### Stage 2: Hands-on Exercises
These exercises are meant to give hands-on experience

**Basics**
1. [ ] [Launch a Single-node Kubernetes Cluster using Minikube](https://www.katacoda.com/courses/kubernetes/launch-single-node-cluster)
1. [ ] [Deploy containers using kubectl](https://www.katacoda.com/courses/kubernetes/kubectl-run-containers)
1. [ ] [Deploy Containers using YAML](https://www.katacoda.com/courses/kubernetes/creating-kubernetes-yaml-definitions)
1. [ ] [Getting Started with Kubeadm](https://www.katacoda.com/courses/kubernetes/getting-started-with-kubeadm)
1. [ ] [Helm Package Manager](https://www.katacoda.com/courses/kubernetes/helm-package-manager)

**Intermediate**
1. [ ] [Networking in Kubernetes](https://www.katacoda.com/courses/kubernetes/networking-introduction)
1. [ ] [Ingress Routing](https://www.katacoda.com/courses/kubernetes/create-kubernetes-ingress-routes)
1. [ ] [Running Stateful Services with Kubernetes (Storage)](https://www.katacoda.com/courses/kubernetes/storage-introduction)
1. [ ] [Managing Secrets](https://www.katacoda.com/courses/kubernetes/managing-secrets)
1. [ ] [Troubleshooting Kubernetes Applications](http://troubleshooting.kubernetes.sh/)

**Advanced**
1. [ ] [Kelsey Hightower's Kubernetes the hard way](https://github.com/kelseyhightower/kubernetes-the-hard-way)


### Stage 3: Projects

1. [ ] Google Cloud: Set up a Cluster using `gcloud` command tool (https://cloud.google.com/kubernetes-engine/docs/how-to/creating-a-container-cluster); you have access to a Google Cloud project called `gitlab-demo` with your `@gitlab.com` Google Apps account.
    In the Comments, provide the following:
    - The `gcloud` command used to create the cluster
    - Dump current cluster state

1. [ ] Create a cluster using [`eksctl` with Cluster Autoscaling enabled](https://docs.aws.amazon.com/eks/latest/userguide/cluster-autoscaler.html)
    In the comments, provide the following:
    - The command used to create the cluster
    - The output after the cluster is created
    - A tail of the Cluster AutoScaler logs

1. [ ] Deploy a sample application to a Cluster, with a Persistent Disk and Service using a YAML file
    In the Comments, provide the following:
    - Content of the YAML file used
    - Describe the Pod, Deployment and Service created

### Stage 4: Pairing Sessions
- [ ] The essence of this pairing sessions is to learn together with other team members while solving customer tickets. You are required to mention 5 pairing sessions you participated and your key take aways from the sessions in the comments area below. (Tick when you have 5)

### Stage 5: Quiz?

- [ ] **Done with Stage 5**

_Need link to Quiz here_

- [ ] Quiz answers were checked by a Kubernetes expect (_insert name here_), who said you passed.

### Final Stage

- [ ] Discuss taking the [CKA Examination](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/) with your manager and Pass the Exam. 
- [ ] Your Manager needs to check this box to acknowledge that you finished this series

/label ~module
