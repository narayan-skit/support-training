---
module-name: "Working On Tickets"
area: "Customer Service"
maintainers:
  - tatkins
---

## Introduction

Welcome to the next module in your Support Onboarding pathway!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

**Goals of this checklist**

When you've completed this module you'll have helped your first customers by pairing with Support Engineers and replying to tickets.

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/), the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **two weeks to complete**.

### Stage 0. Zendesk Basics

You've just completed the Zendesk Basics module. A couple of extra tips that you might find useful:

1. Read about Sensitive information
   - [ ] [Handling Sensitive Information With GitLab Support](https://about.gitlab.com/support/sensitive-information.html)
   - [ ] [Removing Information From Tickets](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#removing-information-from-tickets)
1. [ ] Consider installing the [Zendesk Download Router](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router), which will sort downloaded Zendesk ticket attachments into their own folders. This is particularly useful for Solutions focused Support engineers.
1. [ ] Learn how to [automatically file a documentation improvement GitLab issue through ZenDesk](https://about.gitlab.com/handbook/support/workflows/improving-documentation.html).

### Stage 1. See it all in action!

#### Pairing Sessions

While you can start pairing any time, it is important that you have worked through and finished up until this point so that you can connect what you're learning with what's happening during pairing sessions. As you begin this stage, you should simultaneously be working on the rest of your onboarding including your area(s) of focus.

_Instructions_

- Perform at least **10 pairing sessions** - focus on team members with the same focus (Dotcom/Selfmanaged), but at least 3 pairings should be with team members in the other focus.
- Consider people outside of your immediate region to avoid silos.

1. [ ] Create a Google Calendar event, inviting that person. If you're unsure of times due to timezones, feel free to send them a message in Slack first. When it's time for the pairing session, create a new [Support Pairing project Issue](https://gitlab.com/gitlab-com/support/support-pairing/-/tree/master/.gitlab/issue_templates) and use the appropriate template for the call. 
   - Note: Try to pair with at least 2 people from [SaaS and Self-managed focus](https://gitlab-com.gitlab.io/support/team/areas-of-focus.html), 1 [L&R](https://about.gitlab.com/handbook/support/license-and-renewals/) focused team member, and at least 3 seniors, as they can share their expertise and offer guidance on how to grow.
   1. [ ] Call with your support onboarding buddy; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with your support onboarding buddy; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___

#### Tickets Review   

1. [ ] Check out your team's responses on tickets.
   1. [ ] Hop on to the `#feed_zd-main` channel in Slack and see the tickets as they
      come in and are updated.
   1. [ ] Read through about 20 old tickets that your colleagues have worked on and their responses. A recomendation is not to only read through the currently open tickets, but also through the ones that are in Solved status. You can find those by typing `status:solved` in ZenDesk search bar.

## Congratulations! You've now completed the Support Engineer onboarding pathway.

The next step is to discuss with your manager what your first area of focus is. You should then start the pathway for that area (usually "GitLab.com SAAS support" or "Self-managed support").

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
