---
module-name: "Customer Emergency On-Call"
area: "Customer Service"
maintainers:
  - TBD
---

## Customer Emergency On-Call

_Typically to be completed within 8 weeks of joining._

**Goal** Be familiar with the responsibilities of being on-call for Customer Emergencies

## Stage 1: On-call and Incident Basics

- [ ] Done with Stage 1

1. [ ] Read about on-call duty:
   1. [ ] GitLab's [on-call guide](https://about.gitlab.com/handbook/on-call/). On that
   page, the [Customer Emergency On-Call Rotation Section](https://about.gitlab.com/handbook/on-call/#customer-emergency-on-call-rotation)
   is what applies to Support Engineers, **NOT** the Production Team section.
   1. [ ] [GitLab Support's On-Call Guide](https://about.gitlab.com/handbook/support/on-call/) contains even more information specific to handling emergencies in Support.
1. [ ] Familiarize yourself with the [Emergency Runbook](https://gitlab.com/gitlab-com/support/emergency-runbook) that you can use during an emergency.
1. [ ] Read the [Incident Management](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/) page from the Infrastructure section of the GitLab handbook to understand how to collaborate with the Site Reliability Engineers on-call for GitLab.com emergencies. Take special note of:
   + [What an incident is](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incident-management).
   + What [roles](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) are assumed during an incident.
   + The [definitions](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#status) of the different state of operations that the GitLab.com platform may be in during an incident

## Stage 2: PagerDuty

- [ ] Done with Stage 2

1. [ ] Sign up on PagerDuty with the link that will be emailed to you, and install the app on your phone.
   1. [ ] Familiarize yourself with the interface and the functionality.
1. [ ] Configure your personal notification rules in PagerDuty under "My Profile" > "Notification Rules"
   1. [ ] Currently, [customer emergency escalation policy](https://gitlab.pagerduty.com/escalation_policies#PKV6GCH) is set to 10 minutes.
      That means if you do not respond to the notification within this period, the emergency will escalate to the rest of the team.
      Make sure your personal notification rules take this into account.
   1. [ ] Remember to update this accordingly when your details changed
1. [ ] Use [this PagerDuty guide](https://support.pagerduty.com/docs/schedules-in-apps#section-export-only-your-on-call-shifts) to subscribe to your on-call schedule.
1. [ ] OPTIONAL: Now that you have access to PagerDuty, consider joining a shadow rotation and getting paged right along with the Support Engineer On-Call! Your manager can help you with this.
1. [ ] OPTIONAL: Watch the training video on [Adding Yourself to Shadow Rotations on PagerDuty](https://drive.google.com/file/d/1W4Y699_a9dVpeQzGaQpys-YDBKRdZckm/view). **NOTE**: Please only do this PagerDuty schedules labeled **Shadow**. Do not add yourself to the primary on-call rotations.

## Stage 3: Reference Architecture, Monitoring and Logs

- [ ] Done with Stage 3

1. [ ] Familiarize yourself with the the [Reference Architectures](https://docs.gitlab.com/ee/administration/reference_architectures/index.html). 
1. [ ] Read through the [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/) document to gain a basic understanding of the infrastructural layout of GitLab.com, note the similarities (and differences) with the self-managed reference architectures.
1. [ ] Read about the [Monitoring of GitLab.com](https://about.gitlab.com/handbook/engineering/monitoring/) to understand how our infrastructure team monitors the performance of GitLab.com.
1. [ ] Read about which [critical dashboards](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#critical-dashboards) show if GitLab.com is experiencing an incident and then bookmark the following ones.
  + [Triage](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s)
  + [General Triage](https://dashboards.gitlab.net/d/general-triage/platform-triage?orgId=1)

1. Learn about finding errors in logs on GitLab.com - Note: Kibana and Sentry sign in use the dev account.
    1. [ ] [Using Kibana](https://about.gitlab.com/handbook/support/workflows/kibana.html).
        - Note: Typical usage is `rails` and `sidekiq` logs. `HAproxy` is only available to infra team. Ask in the #production Slack channel if necessary.
    1. [ ] [Searching Kibana, Sentry, and filing GitLab issues from Sentry](https://about.gitlab.com/handbook/support/workflows/500_errors.html).
    1. [ ] As an exercise, visit a non-existing page on GitLab.com, such as [gitlab.com/gitlab-com/doesntexist](gitlab.com/gitlab-com/doesntexist). Search Kibana for the 404 error using your username. Add a screenshot of the relevant portion of the error you find in Kibana as a comment to this issue.
    1. [ ] Search Sentry using the `correlation_id`. You may not find anything and the search is not reliable, but take a screenshot of your search and results and add it as a comment to this issue anyway.

## Stage 4: Getting Prepared

- [ ] Done with Stage 4
Your workspace should be configured to be as prepared as possible for an incident. This means having essential issue trackers bookmarked, joining incident management related Slack channels, and being aware of where reports of issues from end-users will surface.

### Issue Trackers

1. [ ] Bookmark these issues trackers.
  + [Production](https://gitlab.com/gitlab-com/gl-infra/production/issues)
  + [CMOC Handover](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/issues)
  + [Emergency Runbook](https://gitlab.com/gitlab-com/support/emergency-runbook)

### Slack Channels

1. [ ] Join these Slack channels
  + [#production](https://gitlab.slack.com/messages/C101F3796)
  + [#incident-management](https://gitlab.slack.com/messages/CB7P5CJS1)    

## Stage 5: GitLab.com Access

- [ ] Done with Stage 5

### GitLab.com Admin Access
In many cases, you may not need your admin account. In a screenshare session, you should be able to verify the behavior being described and find
the relevant errors in Kibana or get enough of an idea that you can build reproduction without touching customer data. However, there may also
be times where viewing pipeline output, or other data directly will be vital in resolving the issue.

1. [ ] Create a new GitLab.com account called `username-admin` using your `username+admin@gitlab.com` as the email address.
1. [ ] Set up 2FA on the new account
1. [ ] Open a [Individual Access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) for GitLab.com admin
1. [ ] Tag your manager for approval

While you wait for provisioning, read and check off your understanding of the following statements:

- [ ] I will use this access only in the course of doing work on a Support ticket or incident.
- [ ] I will not access customer data without a direct URL to that data provided by the customer.
- [ ] I will not share information that the user (or users) I'm working with would not have access to, even if it's about another user or project within their organization.
- [ ] I will not take any actions that a user could reasonably take themselves, even if directed to do so by the user. 
- [ ] I will use my non-admin account if I need to reproduce customer issues

#### Other .com admin tips:
 - [ ] Set a different color scheme in your admin account from your [account preferences](https://gitlab.com/profile/preferences)
 - [ ] Use a different browser or browser profile for your admin account
 - [ ] If you need to create an access token, set a short expiration date (be aware that GitLab.com uses UTC, so tokens might expire at a different time than you expect!)

### GitLab.com Staging Access
You may use staging.gitlab.com in the process of verifying a patch generated in the scope of an emergency before pushing it out to production.
You don't need access to any special account type, a normal user account should be sufficient.

1. [ ] Open a [Individual Access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) for staging.gitlab.com

## Stage 6: Getting Scheduled for On-Call
- [ ] Done with Stage 6

1. [ ] Ping your manager to add you to the PagerDuty Rotation.
   * [ ] Manager: add the Engineer to the end of the user list, with at least 6 weeks of lead time before their first on-call shift.
1. [ ] Schedule time with your trainer to have them show you how to respond to Customer Emergencies.
1. [ ] In the weeks leading up to your first on-call shift, shadow the on-call engineers who are handling customer emergencies.
   * [ ] ZD Ticket: ___
   * [ ] Repeat this as many times as you are able.
1. [ ] (Optional) Have your onboarding buddy or an available team member reverse shadow you on a customer emergency call (*you will be leading the call*).
   * [ ] ZD Ticket: ___
1. [ ] After your first on-call shift, create an MR to add the "On-call champion!" moniker to your entry on the [GitLab team page](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/tree/master/-/data/team.yml/):
     ```expertise: | <li><a href="/handbook/on-call/">On-call</a> champion!</li>```

/due in 8 weeks
