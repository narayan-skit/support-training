---
module-name: "Geo"
area: "Product Knowledge"
gitlab-group: "Enablement:Geo"
maintainers:
  - TBD
---

**Title:** _"Geo - **your-name**"_

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

**Goal of this checklist:** Set a clear path for Geo Expert training

### Stage 1: Commit and become familiar with what Geo is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] In your Slack Notification Settings, set **Geo** as a **Highlight Word**
1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] Commit to this by notifying the current experts that they can start
   routing non-technical Geo questions to you
1. [ ] Read about the initial decisions, implementation and background
   1. [ ] GitLab University (Be aware that we have added a lot of features since those materials and they might not be up to date but are great in providing context)
      1. [ ] [GLU Deck on Geo](https://drive.google.com/open?id=1r9nXLxU9fuAJdEDTi0ZWg7JyU5Ng4hNJPY3jnQyaor8)
      1. [ ] [GLU Recording on Geo](https://drive.google.com/open?id=0BxSd33hPSs-jRGRLUkpuWHF2cjQ)
   1. [ ] Read the [detailed "How we built GitLab Geo" blog post](https://about.gitlab.com/2018/09/14/how-we-built-gitlab-geo/) for details about implementation and phases we went through
   1. [ ] Watch 3 videos (_Geo Scheduling Call_, _Geo Monthly Testing Update_) and any other from [Geo Group on YouTube](https://www.youtube.com/playlist?list=PL05JrBw4t0KoY_6FXXVgj7wPE9ZDS4cOw).
1. [ ] Read through all the [Geo Documentation](https://docs.gitlab.com/ee/gitlab-geo/README.html)
1. [ ] Watch Aric's [Geo Deep Dive](https://drive.google.com/open?id=1P365fu_wjNRnJLGyCwq_giFnsIoCIEoI)
1. [ ] Read through the [Geo Thought-provoking Questions](https://docs.google.com/document/d/1j83NkLkIJwTYKO2tS471Pw4MvePLLiOOptfrfkbomyk/edit?usp=sharing). Keep these in mind as you work through the module. You should be able to answer them by the end.

### Stage 2: Technical Setup

- [ ] **Done with Stage 2**

Remember to contribute to any documentation that needs updating

1. [ ] Standard installations following the [Setup Instructions](https://docs.gitlab.com/ee/administration/geo/replication/index.html#setup-instructions)
   to install Geo on two VM's
1. [ ] Set up a local repo to push to the primary and pull from the secondary
1. [ ] Set up a local repo to push to the secondary and pull from the secondary
1. [ ] Destroy the primary and promote the secondary node to primary by following
   the [GitLab Geo Disaster Recovery Instructions](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/index.html)
   1. [ ] Connect a local repo to this new primary and make sure you can push and pull
1. [ ] Test HTTP/HTTPS to make sure Geo is working after each step

    If you are using self-signed certificate checkout [mkcert](https://github.com/FiloSottile/mkcert) to make this an easy process.
   1. [ ] Set up [custom / self signed certificates](http://docs.gitlab.com/omnibus/common_installation_problems/README.html#using-self-signed-certificate-or-custom-certificate-authorities)
   1. [ ] Start with HTTP on both servers and change to the following:
   1. [ ] Primary use HTTP, secondary HTTPS
   1. [ ] Primary use HTTPS, secondary HTTP
   1. [ ] HTTPS on both
   1. [ ] Back to HTTP
1. [ ] [Update your Geo setup](https://docs.gitlab.com/ee/administration/geo/replication/version_specific_updates.html) 3 times and make sure at least one of them is a new minor version.
   1. [ ] from version __ to version __
   1. [ ] from version __ to version __
   1. [ ] from version __ to version __
1. [ ] [Set up a Docker Registry on a secondary](https://docs.gitlab.com/ee/administration/geo/replication/docker_registry.html) Geo node to mirror the primary one
1. [ ] Set up custom ports for SSH and HTTP
1. [ ] Upgrade and Downgrade
   1. [ ] Start with GitLab CE
   1. [ ] Upgrade to GitLab EE
   1. [ ] Configure Geo
   1. [ ] Disable Geo after a trial period
   1. [ ] Downgrade back to CE

### Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Contribute valuable responses on at least 5 Geo tickets, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay it to
the customer.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 4: Pair on Customer Calls

- [ ] **Done with Stage 4**

1. [ ] Installation calls where we help a client install Geo
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___

### Stage 5: Quiz

- [ ] **Done with Stage 5**

1. [ ] Request access to the [Geo Module Quiz](https://docs.google.com/document/d/1t73EdaiNUtn4NVH3-Dae01Whd6eEFQFnP43fS_c1HA0/edit?usp=sharing) from your manager or a Geo Expert
1. [ ] Quiz answers were checked by one of the current Geo Experts (have a look at the [team page](https://about.gitlab.com/company/team/?department=support-department) to find them), and they said you passed

### Final Stage

- [ ] Your Manager needs to check this box to acknowledge that you finished
- [ ] Send a MR to declare yourself a **Geo Expert** on the team page

/label ~module
