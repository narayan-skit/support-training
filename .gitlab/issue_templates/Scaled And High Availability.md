---
module-name: "Scaled And High Availability"
area: "Product Knowledge"
gitlab-group: "Enablement:Distribution"
maintainers:
  - TBD
---

**Title:** _"GitLab HA - **your-name**"_

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

**Goal of this checklist:** Set a clear path for High Availability Expert training

In addition to the suggested actions here you might like to refer to [Wei-Meng's HA book](https://gitlab.com/weimeng/gitlab-ha-book/-/blob/master/gitlab-ha.ad) (currently WIP 2020-03)

### Stage 1: Commit and Become familiar with what High Availability is

- [ ] **Done with Stage 1**

   1. [ ] Ping your manager on the issue to notify that you have started
   1. [ ] In your Slack Notification Settings, set **HA** as one of **My Keywords**
   1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
   1. [ ] Commit to this by notifying the current experts that they can start routing non-technical HA questions to you
   1. [ ] Understand the [basic concept of high availability](https://www.digitalocean.com/community/tutorials/what-is-high-availability)
   1. [ ] Understand the use of [GitLab High Availability roles](https://docs.gitlab.com/omnibus/roles/)
   1. [ ] Understand the different [HA architectures possible with GitLab](https://docs.gitlab.com/ee/administration/high_availability/README.html#architecture-examples)

### Stage 2: Technical Setup

- [ ] **Done with Stage 2**

   1. [ ] Set up a minimal [HA setup using the GitLab omnibus package](https://docs.gitlab.com/ee/administration/high_availability/)
      1. [ ] [Configure the database](https://docs.gitlab.com/ee/administration/high_availability/database.html)
      1. [ ] [Configure Redis](https://docs.gitlab.com/ee/administration/high_availability/redis.html)
      1. [ ] [Configure NFS](https://docs.gitlab.com/ee/administration/high_availability/nfs.html)
      1. [ ] [Configure the GitLab Application servers](https://docs.gitlab.com/ee/administration/high_availability/gitlab.html)
      1. [ ] [Configure the load balancers](https://docs.gitlab.com/ee/administration/high_availability/load_balancer.html)

    Note: A minimal setup should have 2 application servers, 7 nodes for database components and 1 load balancer.

### Stage 3 (WIP): GitLab HA Administration

- [ ] **Done with Stage 3**

Remember to contribute to any documentation that needs updating

### Stage 4: Tickets

- [ ] **Done with Stage 4**

- [ ] Contribute valuable responses on at least 10 HA tickets, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 5 (WIP): Customer Calls

- [ ] **~~Done with Stage 5~~**

### Stage 6 (WIP): Quiz?

- [ ] **~~Done with Stage 6~~**

### Final Stage

- [ ] Your manager needs to check this box to acknowledge that you have finished.
- [ ] Send a MR to declare yourself an **High Availability Expert** on the team page.

/label ~module
