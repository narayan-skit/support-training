---
module-name: "Documentation"
area: "Product Knowledge"
gitlab-group: "Create:Editor"
maintainers:
  - TBD
---

### Overview

**Goal**: You feel proficient at making a documentation merge request.

*Length*: hours (maybe? please update with a better estimate)

**Objectives**: At the end of this module, you should be able to:

- open a merge request specifically for documentation.
- follow the documentation process.
- find who the Technical Writer counterpart for the relevant group the merge request is related to.

### Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: Documentation Training - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Commit to this by adding "Documentation" as a [knowledge area for yourself](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

### Stage 1: Introduction

1. [ ] Watch this [introduction to docs video](https://www.youtube.com/watch?v=BlaZ65-b7y0) ([Slides](https://docs.google.com/presentation/d/11-_w_iIhucMbdZ5ybKAFFGpqDAOtWepP5TZ3RtgU8Ik/edit#slide=id.g3db14447a7_0_0)) to get a quick overview. Please keep in mind this was made for external contributors.
2. [ ] Read about [the Docs-first methodology](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html#docs-first-methodology) and [why our documentation as single source of truth](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html#why-a-single-source-of-truth).
3. [ ] Familiarize yourself with the [Ticket deflection through documentation Support workflow](https://about.gitlab.com/handbook/support/#ticket-deflection-through-documentation).

#### Where to ask for help

If at any time, you have questions or need help getting a MR started or completed, please reach out.

- [#docs](https://gitlab.slack.com/archives/C16HYA2P5)
- [#git-help](https://gitlab.slack.com/archives/C1E21S2LD)
  - specifically for `git` command line issues
- [#support_team-chat](https://gitlab.slack.com/archives/CCBJYEWAW)
  - You can ping those listed as knowledgeable in "Documentation" or one of the Docs counterparts.

### Stage 2: Your first docs MR!

There is a lot of information surrounding documentation and how to do it, but the best way to learn is to start with a small MR and get right into it.

1. [ ] Find a super small fix to do. This might be a typo, format issue, small wording change. For this first MR, try not to pick a technical change.
    - If you can't find anything, you can also consider a [documentation issue that's good for new contributors](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=documentation&label_name[]=good%20for%20new%20contributors).
1. [ ] Make the change via whatever editor you want (local, WebIDE, Web Editor).
    - Tip: While viewing the page on the docs site, scroll to the bottom and click on "Edit this page" to view the repo file.
    - Branch name should start with `docs-` (or [one of other options listed in the branch name scheme](https://docs.gitlab.com/ee/development/documentation/#branch-naming)).
    - Make sure the commit message follows our [commit message guideliens](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#commit-messages-guidelines), in particular:
        1. 3 or more words.
        1. Start with a capital letter.
        1. No period at the end.
1. [ ] When you open the merge request, [as per our docs MR guidelines](https://docs.gitlab.com/ee/development/documentation/#merge-requests-for-gitlab-documentation), make sure to choose "Documentation" as the template.
1. [ ] Fill in the template as best you can, the minimum:
    1. Description on change.
    1. Link to ticket/issue if relevant.
    1. Add appropriate labels. Minimum:
        - documentation
        - stage
        - group
        - [Support Team Contributions](https://about.gitlab.com/handbook/support/#support-fixes)
        - customer (if related to ticket)
    1. Assign to the relevant Technical Writer (TW).
        - Find the "Technical Writer" counterpart on the [product categories page](https://about.gitlab.com/handbook/product/product-categories/#devops-stages).
        - If it's not a product stage/group (such as GDK), use [the technical writing assignments page](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments-to-other-projects-and-subjects).
    1. Make sure to check off:
        - `Delete source branch when merge request is accepted.`
        - `Squash commits when merge request is accepted.` "Apply suggestion" is used frequently in docs MRs so this is preferred.
1. [ ] Submit!
    - Your first docs MR: LINK HERE
    - Once it's been reviewed by the assigned TW, you'll have finished your first docs MR. Congratulations!

### Stage 3: Guidelines

Let's now take the time to read up on some of the relevant guidelines in a bit more detail:

1. [ ] Go over the [Documentation workshop slides from Contribute 2020](https://docs.google.com/presentation/d/1QrO-XQ7RfZ6OD29lRt5L42dxOoJFEienag_VRS_rwsE/edit#slide=id.g55e9452978_0_75).
1. [ ] All documentation guidelines are under the [Contributor > Documentation section](https://docs.gitlab.com/ee/development/documentation/). You don't need to read everything, but take a browse through to know what's there.
1. [ ] Briefly read over the [structure page](https://docs.gitlab.com/ee/development/documentation/structure) with a more careful read of the [troubleshooting section](https://docs.gitlab.com/ee/development/documentation/structure#troubleshooting).
1. [ ] Read through the [documentation process](https://docs.gitlab.com/ee/development/documentation/workflow.html).
    - Note: Every docs page should have a "Troubleshooting" section at the bottom for Support to add anything that doesn't fit in the regular content area.
1. [ ] Read the [post merge review guidelines](https://docs.gitlab.com/ee/development/documentation/workflow.html#post-merge-reviews).
    - When would you use this in Support: Got an urgent docs MR? At times, we may want a docs fix as soon as possible.
    - If it's in the "Troubleshooting section" of any page, follow the guidelines except assign to a support manager who is online who will do a quick review and merge.
    - If it's not, then depending on how quickly you need it, throw it in #docs Slack asking anyone in #docs to review and merge ASAP,
    or if it can wait until the assigned TW will be online, ping them in #docs with a link to the MR. Since this will be reviewed by a TW, this does not require a post merge review.
1. [ ] Read about [docs deploy](https://docs.gitlab.com/ee/development/documentation/site_architecture/index.html#deploy-the-docs-site). The key thing is to note how often docs are deployed.
    1. [ ] Optionally, read the rest of the [site architecture information](https://docs.gitlab.com/ee/development/documentation/site_architecture/index.html) to learn how the docs site is built.

#### Style and linters

1. [ ] Read the [documentation style guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html).
    - No one expects you to memorize all the style guidelines! Read through it once and try to remember what's covered so that you can reference it later.
1. While not required, it is highly recommend to [install the docs linters](https://docs.gitlab.com/ee/development/documentation/testing.html#local-linters) in your local editor to prevent pipelines from failing.
    1. [ ] Set up markdownlint.
    1. [ ] Set up Vale.
    1. [ ] [Set up a vertical ruler](https://stackoverflow.com/questions/29968499/vertical-rulers-in-visual-studio-code) to help you [split long lines](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html#text).

### Stage 4: From tickets to documentation

Whenever a customer says the docs are confusing or unclear, we should be making a merge request to resolve their concern.
If you're not sure what to put in the MR, then you can instead create a documentation issue

1. [ ] Review 5 tickets where documentation was created as a result of the ticket. You can search in ZenDesk for `tags:document_this` or [view documentation support contribution MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=documentation&label_name[]=Support%20Team%20Contributions).
    1. [ ] __
    1. [ ] __
    1. [ ] __
    1. [ ] __
    1. [ ] __
1. [ ] Read over how the [Document this](https://about.gitlab.com/handbook/support/workflows/improving-documentation.html) function in ZenDesk works.
1. [ ] Alternatively, you can [create issues with the documentation template](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Documentation) manually.
1. Make at least 2 docs MRs as a result of customer tickets:
    1. [ ] Docs MR that adds content (usually troubleshooting section): LINK
    1. [ ] Docs MR that fixes inaccuracy: LINK
1. Optional: Next level documentation MR "administration". In addition to what was previously covered:
    1. [ ] Add the appropriate [scoped docs label](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=docs%3A%3A)
    1. [ ] Add a milestone (choose current milestone unless less than 3 days away, use next milestone)

### Penultimate Stage: Review

You feel that you can now do all of the objectives:

- open a merge request specifically for documentation.
- follow the documentation process.
- find who the Technical Writer counterpart for the relevant group the merge request is related to.

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself!

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your trainer and manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Once complete, add this module to the list of training you have completed!
1. [ ] Consider also taking the [Code Contributions training](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Code%20Contributions.md) which focuses on GitLab code changes.

/label ~module
