---
module-name: "Continuous Integration"
area: "Product Knowledge"
gitlab-group: "Verify:Continuous Integration"
maintainers:
  - TBD
---

**Title:** _"GitLab CI - your-name"_

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

**Goal of this checklist:** Set a clear path for GitLab-CI training

### Stage 1: Commit and Become familiar with what GitLab CI is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical GitLab CI questions to you
1. [ ] Commit to this by adding it to your [knowledge areas](https://gitlab-com.gitlab.io/support/team/skills-by-person.html) by updating the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml).
1. [ ] GitLab University
   1. [ ] [Getting started with GitLab and GitLab CI](https://docs.gitlab.com/ee/ci/README.html#introduction-to-gitlab-cicd). Read the articles from Introduction to GitLab CI/CD section.
   1. [ ] [The Basics of GitLab CI](https://about.gitlab.com/blog/2016/07/29/the-basics-of-gitlab-ci/)
   1. [ ] [Continuous Integration, Delivery, and Deployment with GitLab](https://about.gitlab.com/blog/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/)
   1. [ ] [10 Things to Know About GitLab CI](https://docs.google.com/presentation/d/1g2trciavyQpBelji6OkMottVB9zCmzxTIcIP-GY2Uf4/edit#slide=id.g56924a4052_6_0)
   1. [ ] [GitLab Container Registry](https://about.gitlab.com/blog/2016/05/23/gitlab-container-registry/)
   1. [ ] [GitLab & Docker - Recording](https://www.youtube.com/watch?v=ugOrCcbdHko&index=12&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e)
1. [ ] [CI Training Video by Jose Tores](https://drive.google.com/file/d/0B5OISI5eJZ-DSzA5THdvMG9Jbm8) - [Slides](https://gitlab-org.gitlab.io/ci-training-slides/#/) - [Example App](https://gitlab.com/gitlab-org/ci-training-sample)
1. [ ] [CI Backend Architecture walkthrough video](https://youtu.be/ew4BwohS5OY) | [Diagram](https://drive.google.com/file/d/1LFl-KW4fgpBPzz8VIH9rsOlAH4t0xwKj/view) - watch to learn how everything connects together.

### Stage 2: Basic CI

- [ ] **Done with Stage 2**

1. [ ] Read the Table Of Contents for the [.gitlab-ci.yml docs](https://docs.gitlab.com/ee/ci/yaml/README.html)
   and make sure you know the meaning of every heading.
1. [ ] Set up a repository on GitLab.com and create a .gitlab-ci.yml file to
   get a pipeline running using the free shared runners on GitLab.com (this can
   be a basic toy project).
1. [ ] Note that your builds may show as **Pending** until a shared runner becomes
   available (the next step will solve this).
1. [ ] Install your own Runner (locally or hosted somewhere) following the [Setup Instructions](https://docs.gitlab.com/runner/install/)
1. [ ] Read about the different [Executors](https://docs.gitlab.com/runner/executors/)
1. [ ] Register your runner as a [Specific Runner](https://docs.gitlab.com/ee/ci/runners/README.html#registering-a-specific-runner)
   on your project, and test that it now runs the builds for your project.
1. [ ] Create an example project on your own GitLab instance.
1. [ ] Register a shared runner on your GitLab instance and make sure it can
   successfully run the build for your project.
1. [ ] Optional, for GitLab.com: Watch [this video](https://drive.google.com/file/d/17pjKta4qfTyHcyy9qVpSNQhmysYBRZTp) on ways to troubleshoot runner on GitLab.com. Refer to [this issue for questions and links](https://gitlab.com/gitlab-com/support/dotcom/dotcom-meta/issues/58#note_80876655). Note: As it was recorded August 2018, some links may be out of date.

### Stage 3: Intermediate CI

- [ ] **Done with Stage 3**

1. [ ] Read about [Pipelines and Jobs](https://docs.gitlab.com/ee/ci/pipelines.html).
1. [ ] Check how to [see the status of Pipeline and Jobs](https://docs.gitlab.com/ee/ci/quick_start/README.html#seeing-the-status-of-your-pipeline-and-jobs).
1. [ ] Read about using [Docker Images](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html) with GitLab CI.
1. [ ] [Register a Docker Runner](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#register-docker-runner)
   and change your `.gitlab-ci.yml` to use a Docker image for your language of
   choice.
1. [ ] Read about [Artifacts](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html).
1. [ ] Change `.gitlab-ci.yml` to [create artifacts](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html#defining-artifacts-in-gitlab-ci-yml).
1. [ ] Set the Maximum Artifacts size in Admin and create artifacts above the limit, then check the build log for errors.
1. [ ] Read about [Multi-project pipelines](https://about.gitlab.com/2018/10/31/use-multiproject-pipelines-with-gitlab-cicd/).
1. [ ] Change your `.gitlab-ci.yml` to trigger a pipeline in another project.
1. [ ] Review the Runner [Troubleshooting guide and try to reproduce issues where possible](https://docs.gitlab.com/runner/faq/).
1. [ ] Read the rest of the [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/README.html).
1. [ ] Set up a project with a basic web application that has some tests. Your CI script should deploy it to Heroku automatically when all the tests pass.
1. [ ] Create a basic GitLab Pages project on GitLab.com and make sure the CI script builds it and that the website is visible afterwards.

### Stage 4: Advanced CI

Please ensure that you review with your trainer/manager if they would like you to do all the tasks in this stage.

- [ ] **Done with Stage 4**

1. [ ] Set up a [container registry](https://docs.gitlab.com/ee/user/project/container_registry.html) for your project.
1. [ ] [Build a Docker image and upload it](https://docs.gitlab.com/ee/user/project/container_registry.html#build-and-push-images) to the project registry as part of the CI pipeline.
1. [ ] [Add or integrate a Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/) to your project.
1. [ ] Deploy your [application to Kubernetes](https://medium.com/john-lewis-software-engineering/deploying-to-google-kubernetes-engine-from-gitlab-ci-feaf51dae0c1) via GitLab CI using the Docker container you built. One option is to [customize the Auto DevOps template](https://docs.gitlab.com/ee/topics/autodevops/#customizing).
1. [ ] Set up [Review Apps](https://docs.gitlab.com/ee/topics/autodevops/#customizing) to be deployed to Kubernetes.
1. [ ] Create a [custom image](https://docs.docker.com/compose/rails/) to speed up your build times by having the    dependencies pre-installed (the same way it is done for GitLab CE and EE). See    it in action on the [first line here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab-ci.yml).
1. [ ] Set up a [manual deploy step](https://docs.gitlab.com/ee/ci/yaml/#whenmanual), so that it does not deploy to production    unless someone clicks the button to do so.
1. [ ] Speed up a pipeline using [caching](https://docs.gitlab.com/ee/ci/yaml/#cache).

### Stage 5: Tickets

- [ ] **Done with Stage 5**

1. [ ] Answer 20 GitLab CI tickets and paste the links here, even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay it to the customer.

   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __

### Stage 6: Pair on Customer Calls

- [ ] **Done with Stage 6**

1. [ ] Participate on two customer calls regarding Sales or Troubleshooting GitLab CI.
   1. call with ___
   1. call with ___

### Final Stage

1. [ ] Have your trainer and manager review this issue.
1. [ ] Manager: Have a CI expert in Support review 5-7 of the tickets from Stage 5 and report back to you on whether sufficient CI knowledge has been demonstrated.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit a MR to declare yourself a **CI Expert** on the team page.
1. [ ] Add this module to the list of training you have completed!

/label ~module
